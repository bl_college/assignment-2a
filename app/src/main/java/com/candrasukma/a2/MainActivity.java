package com.candrasukma.a2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int SENDING = 1;
    public static final int CONNECTING = 2;
    public static final int ERROR = 3;
    public static final int SENT = 4;
    public static final int SHUTDOWN = 5;
    public static final String ipServer = "192.168.1.151";
    public final String TAG = this.getClass().getSimpleName().toString();
    TextView textOut;
    EditText textIn;
    Button btnSend;
    String sentence;
    String modifiedSentence;

    String outText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
                sendAction();
                break;
        }
    }

    private void initView() {
        textOut = (TextView) findViewById(R.id.text_out);
        textIn = (EditText) findViewById(R.id.text_in);
        btnSend = (Button) findViewById(R.id.btn_send);
        btnSend.setOnClickListener(this);

    }

    private void sendAction() {
        startServerSocket();
    }

    private void startServerSocket() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Socket clientSocket = new Socket(ipServer, 6789);
                    DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
                    BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    try {
                        while (true) {
                            sentence = textIn.getText().toString();
                            outToServer.writeBytes(sentence + '\n');
                            modifiedSentence = inFromServer.readLine();
                            outText = outText + modifiedSentence + '\n';
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    textOut.setText(outText);
                                    textIn.setText("");
                                }
                            });

                            if(modifiedSentence.equalsIgnoreCase("exit")){
                                clientSocket.close();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        textOut.setText("");
                                        textIn.setText("");
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

}
